#!/bin/bash

path="."
format="mp4"
quality="h"

if [[ $1 != "" ]]; then
    path=$1
fi

if [[ $2 != "" ]]; then
    format=$2
fi

if [[ $3 != "" ]]; then
    quality=$3
fi

docker run --rm -it \
-v "$path:/manim" \
manimcommunity/manim \
manim -q$quality --format=$format scene.py
