## Create video of Bun
```
./main.bash path format quality
```
path -- путь до папки с файлом scene.py. Default = .  
format -- формат видео (mp4, gif). Default = mp4  
quality -- разрешение видео (l, m, h). Default = h  


## Create video animation of Bun

```
docker run --rm -it \
-v "C:/Users/maxim/Programming/de-labs:/manim" \
manimcommunity/manim \
manim -qh scene.py
```

## Create gif animation of Bun

```
docker run --rm -it \
-v "C:/Users/maxim/Programming/de-labs:/manim" \
manimcommunity/manim \
manim -qh --format=gif scene.py
```
