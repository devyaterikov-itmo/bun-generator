from manim import *


class Bun(ThreeDScene):
    def scene_setup(self):
        self.set_camera_orientation(phi=75 * DEGREES, theta=30 * DEGREES)
        self.begin_ambient_camera_rotation(rate=0.3)
        self.axes = ThreeDAxes(x_range=[-4,4], x_length=8)

        self.bun_radius = 2
        self.base_height = 3
        self.cap_height = 2
        self.center = (0, 0, -2)

    def graphic_func(self, x0, k, func):
        return x0 + k * func()

    def get_x_func(self, v):
        return lambda: 1 - np.sqrt(np.sin(v) ** 10)

    def get_y_func(self, v):
        return lambda: 1 - np.sqrt(np.cos(v) ** 10)

    def get_face(self, face_func):
        return Surface(
            face_func,
            u_range=[0, 1],
            v_range=[0, PI/2],
            resolution=16,
        ).set_color(LIGHT_BROWN).set_opacity(1)

    def face1_func(self, p0, xyk_func, zk):
        return lambda u, v: self.axes.c2p(
            self.graphic_func(p0[0], xyk_func(u), self.get_x_func(v)),
            self.graphic_func(p0[1], xyk_func(u), self.get_y_func(v)),
            self.graphic_func(p0[2], zk, lambda: u),
        )

    def face2_func(self, p0, xyk_func, zk):
        return lambda u, v: self.axes.c2p(
            self.graphic_func(p0[0], -xyk_func(u), self.get_x_func(v)),
            self.graphic_func(p0[1], xyk_func(u), self.get_y_func(v)),
            self.graphic_func(p0[2], zk, lambda: u),
        )

    def face3_func(self, p0, xyk_func, zk):
        return lambda u, v: self.axes.c2p(
            self.graphic_func(p0[0], xyk_func(u), self.get_x_func(v)),
            self.graphic_func(p0[1], -xyk_func(u), self.get_y_func(v)),
            self.graphic_func(p0[2], zk, lambda: u),
        )

    def face4_func(self, p0, xyk_func, zk):
        return lambda u, v: self.axes.c2p(
            self.graphic_func(p0[0], -xyk_func(u), self.get_x_func(v)),
            self.graphic_func(p0[1], -xyk_func(u), self.get_y_func(v)),
            self.graphic_func(p0[2], zk, lambda: u),
        )

    def get_cap(self, base_height, cap_height):
        p0 = (self.center[0], self.center[1], self.center[2] + base_height)
        xyk_func = lambda u: self.bun_radius * (1 - u ** 4)
        zk = cap_height

        return VGroup(
            self.get_face(face_func=self.face1_func(p0, xyk_func, zk)),
            self.get_face(face_func=self.face2_func(p0, xyk_func, zk)),
            self.get_face(face_func=self.face3_func(p0, xyk_func, zk)),
            self.get_face(face_func=self.face4_func(p0, xyk_func, zk)),
        )

    def get_base(self, base_height):
        xyk_func = lambda u: self.bun_radius
        zk = base_height

        return VGroup(
            self.get_face(face_func=self.face1_func(self.center, xyk_func, zk)),
            self.get_face(face_func=self.face2_func(self.center, xyk_func, zk)),
            self.get_face(face_func=self.face3_func(self.center, xyk_func, zk)),
            self.get_face(face_func=self.face4_func(self.center, xyk_func, zk)),
        )

    def construct(self):
        self.scene_setup()

        start_base_height = 0.2
        start_cap_height = 0.4

        st_base = self.get_base(base_height=start_base_height)
        st_cap = self.get_cap(
            base_height=start_base_height,
            cap_height=start_cap_height,
        )

        base = self.get_base(base_height=self.base_height)
        cap = self.get_cap(
            base_height=self.base_height,
            cap_height=self.cap_height,
        )

        self.add(st_cap, st_base)
        self.play(Transform(st_cap, cap), Transform(st_base, base), run_time=10)
        self.wait(10)
